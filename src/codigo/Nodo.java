/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

/**
 *
 * @author esteb
 */
public class Nodo {
    private Sitios dato=null;
    private Nodo izq=null;
    private Nodo der =null;
    private int altura=0;

    public Nodo(Sitios dato) {
        this.dato=dato;
    }

   
    public Sitios getDato() {
        return dato;
    }

   
    public void setDato(Sitios dato) {
        this.dato = dato;
    }

   
    public Nodo getIzq() {
        return izq;
    }

    
    public void setIzq(Nodo izq) {
        this.izq = izq;
    }

   
    public Nodo getDer() {
        return der;
    }

    
    public void setDer(Nodo der) {
        this.der = der;
    }

    
    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    
    
}
