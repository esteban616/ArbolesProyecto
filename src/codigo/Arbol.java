/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.List;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author esteb
 */
public class Arbol {

    Nodo raiz = null;
    LinkedList<Nodo> listaHojas = null;

    public void ingresar(LinkedList<LinkedList<Sitios>> dato) {
        ingresar(dato, raiz);
    }

    private void ingresar(LinkedList<LinkedList<Sitios>> dato, Nodo aux) {
        if (raiz == null) {
            raiz = new Nodo(dato);
        } else {

            int altActual = altura(aux.getIzq()) - altura(aux.getDer());

            if (altActual >= -1 && altActual <= 1) {
                double direecion = Math.random();
                if (direecion > 0.5) {
                    if (aux.getIzq() == null) {
                        aux.setIzq(new Nodo(dato));
                        aux.getIzq().setPadre(aux);
                    } else {
                        ingresar(dato, aux.getIzq());
                    }
                } else {
                    if (aux.getDer() == null) {
                        aux.setDer(new Nodo(dato));
                        aux.getDer().setPadre(aux);
                    } else {
                        ingresar(dato, aux.getDer());
                    }
                }
            }

            if (altActual >= 2) {
                if (aux.getDer() == null) {
                    aux.setDer(new Nodo(dato));
                    aux.getDer().setPadre(aux);
                } else {
                    ingresar(dato, aux.getDer());
                }
            }

            if (altActual <= -2) {
                if (aux.getIzq() == null) {
                    aux.setIzq(new Nodo(dato));
                    aux.getIzq().setPadre(aux);
                } else {
                    ingresar(dato, aux.getIzq());
                }
            }

        }

    }

    public int altura(Nodo aux) {
        if (aux == null) {
            return -1;
        } else {
            return 1 + Math.max(altura(aux.getDer()), altura(aux.getIzq()));
        }
    }

    public double promDinero() {
        int total = 0;
        int pDinero = 0;
        return promDinero(raiz, pDinero, total);
    }

    private double promDinero(Nodo aux, int pDinero, int total) {
        if (aux != null) {
            total++;
            for (int i = 0; i < aux.getDato().size(); i++) {
                for (int j = 0; j < aux.getDato().get(i).size(); j++) {
                    pDinero = pDinero + aux.getDato().get(i).get(j).getCosto();

                }
            }
            promDinero(aux.getIzq(), pDinero, total);
            promDinero(aux.getDer(), pDinero, total);

            return pDinero / total;

        }
        return 0;
    }

    public double promLista(LinkedList<Nodo> ruta) {
        double prom = 0;
        if (ruta == null) {
            return 0;
        } else {
            for (int i = 0; i < ruta.size(); i++) {
                for (int j = 0; j < ruta.get(i).getDato().size(); j++) {
                    for (int k = 0; k < ruta.get(i).getDato().get(j).size(); k++) {
                        prom = prom + ruta.get(i).getDato().get(j).get(k).getCosto();

                    }
                }
            }
            return prom / ruta.size();
        }

    }

    public int costoLista(LinkedList<Nodo> ruta) {
        int costo = 0;
        if (ruta == null) {
            return 0;
        } else {
            for (int i = 0; i < ruta.size(); i++) {
                for (int j = 0; j < ruta.get(i).getDato().size(); j++) {
                    for (int k = 0; k < ruta.get(i).getDato().get(j).size(); k++) {
                        costo = costo + ruta.get(i).getDato().get(j).get(k).getCosto();

                    }
                }
            }
            return costo;
        }
    }

    public void recorrer(Nodo aux) {
        if (aux != null) {
            if (aux.getIzq() == null && aux.getDer() == null) {
                listaHojas.add(aux);
            }
            recorrer(aux.getIzq());
            recorrer(aux.getDer());
        }
    }

    public LinkedList<Nodo> rutaMasCara() {
        LinkedList<Nodo> ruta = null;
        LinkedList<Nodo> rutaAux = null;
        for (int i = 0; i < listaHojas.size(); i++) {
            Nodo aux = listaHojas.get(i);
            while (aux != null) {
                rutaAux.add(aux);
                aux = aux.getPadre();
            }
            if (costoLista(rutaAux) > costoLista(ruta)) {
                ruta = rutaAux;
            }
        }
        return ruta;
    }

    public LinkedList<LinkedList<Nodo>> rutasPromedio() {
        LinkedList<LinkedList<Nodo>> rutasProm = null;
        LinkedList<Nodo> ruta = null;
        for (int i = 0; i < listaHojas.size(); i++) {
            Nodo aux = listaHojas.get(i);
            while (aux != null) {
                ruta.add(aux);
                aux = aux.getPadre();
            }
            if (promLista(ruta) - promDinero() < 2000 && promLista(ruta) - promDinero() > -2000) {
                rutasProm.add(ruta);
            }
        }
        return rutasProm;
    }

    public boolean visitas(LinkedList<Nodo> ruta) {
        boolean restaurante = false;
        boolean serviciosP = false;
        boolean visitados = false;
        for (int j = 0; j < ruta.size(); j++) {
            for (int k = 0; k < ruta.get(j).getDato().size(); k++) {
                for (int l = 0; l < ruta.get(j).getDato().get(k).size(); l++) {
                    if (ruta.get(j).getDato().get(k).get(l).getTipo().equalsIgnoreCase("restaurante")) {
                        restaurante = true;
                    }
                    if (ruta.get(j).getDato().get(k).get(l).getTipo().equalsIgnoreCase("servicios publicos")) {
                        serviciosP = true;
                    }
                }

            }
        }
        if (restaurante==true && serviciosP==true) {
            visitados=true;
        }
        return visitados;
    }

    public LinkedList<Nodo> rutaMenor() {
        LinkedList<Nodo> ruta = null;
        LinkedList<Nodo> rutaAux = null;
        for (int i = 0; i < listaHojas.size(); i++) {
            Nodo aux = listaHojas.get(i);
            while (aux != null) {
                rutaAux.add(aux);
                aux = aux.getPadre();
            }
           
            if (visitas(rutaAux)==true && costoLista(ruta) < costoLista(rutaAux)) {
                ruta = rutaAux;
            }

        }
        return ruta;
    }

    public int tiempoRuta(LinkedList<Nodo> ruta) {
        int tiemTotal = 0;
        for (int i = 0; i < ruta.size(); i++) {
            for (int j = 0; j < ruta.get(i).getDato().size(); j++) {
                for (int k = 0; k < ruta.get(i).getDato().get(j).size(); k++) {
                    tiemTotal = tiemTotal + ruta.get(i).getDato().get(j).get(k).getTiempo();
                }

            }
        }
        return tiemTotal;
    }

    public LinkedList<Nodo> rutaTiempo(int tiempo) {
        LinkedList<Nodo> ruta = null;
        LinkedList<Nodo> rutaAux = null;
        for (int i = 0; i < listaHojas.size(); i++) {
            Nodo aux = listaHojas.get(i);
            while (aux != null) {
                rutaAux.add(aux);
                aux = aux.getPadre();
            }
            if (tiempo <= tiempoRuta(rutaAux)) {
                ruta = rutaAux;
            }
        }
        return ruta;
    }
}
